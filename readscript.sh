#!/bin/bash
#Description: Script to read user's input and producce output
#
#Author: Femi Amosu
#
#Date: 2019
#======================================================================


clear

echo -n " What is your favorite color? "


read COLOR

echo -n " What is your favorite city? "

read CITY

echo -n " What is your favorite food? "

read FOOD


sleep 3

echo

echo -e "Your favorite color is: ${COLOR}"
echo
echo -e "Your favorite city is: ${CITY}"
echo
echo -e "and your favorite food is: ${FOOD}"
echo
echo
